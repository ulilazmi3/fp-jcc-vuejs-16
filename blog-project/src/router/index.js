import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '../store/index'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: () => import(/* webpackChunkName: "about" */ '../views/Home.vue')
  },
  {
    path: '/blogs',
    name: 'Blogs',
    component: () => import(/* webpackChunkName: "about" */ '../views/Blogs.vue')
  },
  {
    path: '/kelola',
    name: 'Kelola',
    meta: { kelola: true },
    component: () => import(/* webpackChunkName: "about" */ '../views/Kelola.vue')
  },
  {
    path: '/blog/:id',
    name: 'Blog',
    component: () => import(/* webpackChunkName: "about" */ '../views/Blog.vue')
  },
  { path: '/blog', redirect: '/blogs' },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
})

// Navigation Guards
router.beforeEach((to, from, next) => {
  const guest = store.getters['auth/guest'];
  if (to.matched.some(record => record.meta.kelola)) {
    if (guest) {
      alert("Halaman ini hanya untuk user yang sudah login!")
    } else {
      next()
    }
  } else {
    next() // make sure to always call next()!
  }
})

export default router

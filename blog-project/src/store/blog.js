export default {
    namespaced : true,
    state: {
        blog : undefined,
        page : 1,
    },
    mutations: {
        selectBlog : (state, payload) => {
            state.blog = payload
        },
        setPage : (state, payload) => {
            state.page = payload
        },
    },
    actions : {
        selectBlog : ( {commit} , payload) => {
            commit('selectBlog', payload)
        },
        setPage : ( {commit} , payload) => {
            commit('setPage', payload)
        }
    },
    getters : {
        blog : state => state.blog,
        title : state => state.blog.title,
        description : state => state.blog.description,
        page : state => state.page
    }
}